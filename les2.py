import requests
from pymongo import MongoClient
from bs4 import BeautifulSoup
import time

domain_url = 'https://geekbrains.ru'
blog_url = 'https://geekbrains.ru/posts'

mongo_url = 'mongodb://localhost:27017'
client = MongoClient(mongo_url)
db = client.geekbrains
collection = db.gb_blog


def get_page_strict(soup):
    posts_list = []
    posts_data = soup.find_all('div', class_='post-item')

    for post in posts_data:
        post_dict = {
            'post_url': f"{domain_url}{post.find('a').attrs.get('href')}",
            'post_title': post.find(class_='post-item__title').text,
            'post_date': post.find(class_='small m-t-xs').text
        }

        posts_list.append(post_dict)
    return posts_list


def get_page_soup(url):
    page_data = requests.get(url)
    soup_data = BeautifulSoup(page_data.text, 'lxml')
    return soup_data


def parser(url):
    posts_list = []
    while True:
        soup = get_page_soup(url)
        collection.insert_many(get_page_strict(soup))
        posts_list.extend(get_page_strict(soup))
        try:
            url = soup.find('a', attrs={'rel': 'next'}, text='›').attrs.get('href')
        except AttributeError as e:
            break
        url = f"{domain_url}{url}"
        time.sleep(1)
    return posts_list


result_data = parser(blog_url)


print(1)
