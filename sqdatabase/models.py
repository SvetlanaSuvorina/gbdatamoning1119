from sqlalchemy import (
    Table,
    Column,
    ForeignKey,
    String,
    Integer
)

from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

associative_tag_post = Table('associative_tag_post', Base.metadata,
                             Column('blog_post', Integer, ForeignKey('blogpost.id')),
                             Column('blog_tags', Integer, ForeignKey('tags.id'))
                             )


class BlogPost(Base):
    __tablename__ = 'blogpost'

    id = Column(Integer, primary_key=True, autoincrement=True)
    url = Column(String, unique=True)
    title = Column(String)
    autor = Column(Integer, ForeignKey('autors.id'))
    autors = relationship('Autors', backref='posts')
    tags = relationship('Tags', secondary=associative_tag_post, backref='posts')

    def __init__(self, title, url, tags=[], autor=None):
        self.title = title
        self.url = url
        if tags:
            self.tags.extend(tags)
        if autor:
            self.autor = autor


class Tags(Base):
    __tablename__ = 'tags'
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String)
    url = Column(String, unique=True)

    def __init__(self, title, url):
        self.title = title
        self.url = url


class Autors(Base):
    __tablename__ = 'autors'
    id = Column(Integer, primary_key=True, autoincrement=True)
    url = Column(String, unique=True)
    name = Column(String)

    def __init__(self, url, name):
        self.url = url
        self.name = name
