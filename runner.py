from scrapy.crawler import CrawlerProcess
from scrapy.settings import Settings
from config import INSAT_LOGIN, INSAT_PASS
from geekbrains import settings
from geekbrains.spiders.gb_blog import GbBlogSpider
from geekbrains.spiders.avito import AvitoSpider
from geekbrains.spiders.instagram import InstagramSpider

if __name__ == '__main__':
    crawler_settings = Settings()
    crawler_settings.setmodule(settings)

    process = CrawlerProcess(settings=crawler_settings)
    process.crawl(InstagramSpider, INSAT_LOGIN, INSAT_PASS, 'realdonaldtrump')
    process.start()
